#!/bin/sh

for disk in nginx phpfpm mariadb artisan
do
    VOL=$(aws ec2 create-volume --region eu-west-1 --availability-zone eu-west-1a --size 5 --volume-type gp2 --output json | jq -r '.VolumeId')
    OLDVOL=$(python3 -c "import yaml; oldfile = yaml.load(open('remote/$disk-disk.yaml', 'r')); print(oldfile['spec']['awsElasticBlockStore']['volumeID'])")

    echo "Removing old volume for $disk ($OLDVOL)"

    kubectl delete pv $disk-disk

    aws ec2 delete-volume --volume-id="${OLDVOL}"

    rm -f /tmp/$disk-disk.old.yaml
    mv remote/$disk-disk.yaml /tmp/$disk-disk.old.yaml
    cat > remote/$disk-disk.yaml <<ENDDISK
apiVersion: v1
kind: PersistentVolume
metadata:
  name: $disk-disk
  labels:
    target: $disk
    type: amazonEBS
spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 5Gi
  awsElasticBlockStore:
    volumeID: $VOL
    fsType: ext4
ENDDISK

    kubectl create -f remote/$disk-disk.yaml --validate=false

    echo "Created $disk disk ($VOL)"
done
